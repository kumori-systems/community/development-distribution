//  Copyright 2022 Kumori Systems S.L.
//
//  Licensed under the EUPL, Version 1.2 or – as soon they
//  will be approved by the European Commission - subsequent
//  versions of the EUPL (the "Licence");
//  You may not use this work except in compliance with the
//  Licence.
//  You may obtain a copy of the Licence at:
//
//  https://joinup.ec.europa.eu/software/page/eupl
//
//  Unless required by applicable law or agreed to in
//  writing, software distributed under the Licence is
//  distributed on an "AS IS" basis,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
//  express or implied.
//  See the Licence for the specific language governing
//  permissions and limitations under the Licence.

package clustermanager

import (
  k "kumori.systems/cluster/kdv"
  yaml "encoding/yaml"
)

///////////////////////////////////////////////////////////////////////////////
//        DEFINITION OF CUE STRUCTURES FOR KUCONTROLLER CONFIGURATION        //
///////////////////////////////////////////////////////////////////////////////

// KuController extra configuration to enable access to
// certain services from external Kubernetes (and non-Kumori)
// services
#ExternalServicesAccess: [string]: {
  source: {
    namespace?: string
    label?: {
      key: string
      value: string
    }
  }
  target: {
    owners?: [string]
    deployments?: [string]
  }
}

///////////////////////////////////////////////////////////////////////////////
//    END OF DEFINITION OF CUE STRUCTURES FOR KUCONTROLLER CONFIGURATION     //
///////////////////////////////////////////////////////////////////////////////

#AuthenticationType: "clientcertificate" | "token" | "none"

let pkgCalico = k.#PackageDeployment & {
  package: {
    name: "calico"
    version: "v3.24.5"
    dependencies: []
  }
  configuration: {
    enabled: false
  }
  resources: {}
  deploymentMethod: {}
}

let pkgKumoriCRD = k.#PackageDeployment & {
  package: {
    name: "kumori-crd"
    version: "1.8.0"
    dependencies: []
  }
  configuration: {}
  resources: {}
  deploymentMethod: {}
}

let pkgKumoriVolume = k.#PackageDeployment & {
  package: {
    name: "kumori-volume"
    version: "1.0.1"
    dependencies: []
  }
  configuration: {}
  resources: {}
  deploymentMethod: {}
}

let pkgKumoriCoreDNS = k.#PackageDeployment & {
  package: {
    name: "kumori-coredns"
    version: "3.1.0"
    dependencies: []
  }
  configuration: {}
  resources: {
    coredns: {
      cpu: "100m"
      cpuLimit: ""
      mem: "70Mi"
      memLimit: "170Mi"
    }
  }
  deploymentMethod: {}
}

let pkgKumoriTopology = k.#PackageDeployment & {
  package: {
    name: "kumori-topology"
    version: "3.0.0"
    dependencies: []
  }
  configuration: {}
  resources: {}
  deploymentMethod: {}
}

let pkgKumoriSolutionController = k.#PackageDeployment & {
  package: {
    name: "kumori-solution-controller"
    version: "3.0.0"
    dependencies: []
  }
  configuration: {}
  resources: {}
  deploymentMethod: {}
}

let pkgKumoriController = k.#PackageDeployment & {
  package: {
    name: "kumori-kucontroller"
    version: "3.2.2"
    dependencies: []
  }
  configuration: {
    defaultLivenessTimeout: "10s"
    defaultReadinessTimeout: "10s"
    topologySpreadConstraintsMaxSkew: 1
    revisionHistoryLimit: 5
    forceRebootOnUpdate: false
    defaultDiskRequestSize: 500
    defaultDiskRequestUnit: "Mi"
    defaultVolatileVolumesType: ""
    if config["kumori-kucontroller"].externalServicesAccess != _|_ {
      externalServicesAccess: "\(yaml.Marshal({config["kumori-kucontroller"].externalServicesAccess & #ExternalServicesAccess}))"
    }
  }
  resources: {}
  deploymentMethod: {}
}

let pkgKumoriInbound = k.#PackageDeployment & {
  package: {
    name: "kumori-kuinbound"
    version: "3.2.0"
    dependencies: []
  }
  configuration: {
    minTLSVersion: "v1.2"
    maxTLSVersion: ""
    cipherSuitesTLS: ""
    ecdhCurvesTLS: ""
    hsts: true
  }
  resources: {}
  deploymentMethod: {}
}

let pkgKumoriAdmission = k.#PackageDeployment & {
  package: {
    name: "kumori-admission"
    version: "3.2.4"
    dependencies: []
  }
  configuration: {
    kumorictlSupportedVersions: "1.5.0,1.5.1"
    kumorimgrSupportedVersions: "1.3.5"
    authenticationType: *config["kumori-admission"].authenticationType | #AuthenticationType
    tokenAuthParams: {
      adminPassword: *config["kumori-admission"].tokenAuthParams.adminPassword | "admin"
      develPassword: *config["kumori-admission"].tokenAuthParams.develPassword | "devel"
    }
    clientCertAuthParams: {
      clientCertRequired: *config["kumori-admission"].clientCertAuthParams.clientCertRequired | true
      clientCertValidateWithTrustedCA: *config["kumori-admission"].clientCertAuthParams.clientCertValidateWithTrustedCA | false
      serverCertDir: *config["kumori-admission"].clientCertAuthParams.serverCertDir | ""
    }
  }
  resources: {}
  deploymentMethod: {}
}

let pkgKumoriAlarm = k.#PackageDeployment & {
  package: {
    name: "kumori-kualarm"
    version: "2.0.0"
    dependencies: []
  }
  configuration: {
    enabled: false
    alarmdefinitions: []
    alarmimpl: {
      prometheus: []
    }
    alarms: []
    alarmmanager: {}
  }
  resources: {}
  deploymentMethod: {}
}

let pkgKumoriVolumeController = k.#PackageDeployment & {
  package: {
    name: "kumori-volume-controller"
    version: "2.1.0"
    dependencies: []
  }
  configuration: {
    enabled: false
    volumeTypes: {}
  }
  resources: {}
  deploymentMethod: {}
}

let pkgKumoriIpFiltering = k.#PackageDeployment & {
  package: {
    name: "kumori-ipfiltering"
    version: "0.0.3"
    dependencies: []
  }
  configuration: {
    enabled: false
    rejectIfFailure: false
    timeout: "20ms"
    maxListSize: 50
    loglevel: "info"
  }
  resources: {
    ipfiltering: {
      cpu: "250m"
      cpuLimit: "750m"
      mem: "50Mi"
      memLimit: "150Mi"
    }
  }
  deploymentMethod: {}
}

let pkgKumoriAuthService = k.#PackageDeployment & {
  package: {
    name: "kumori-authservice"
    version: "0.0.1"
    dependencies: []
  }
  configuration: {
    enabled: false
  }
  resources: {
    authservice: {
      cpu: "250m"
      cpuLimit: "750m"
      mem: "50Mi"
      memLimit: "150Mi"
    }
  }
  deploymentMethod: {}
}

let pkgKubePrometheus = k.#PackageDeployment & {
  package: {
    name: "kube-prometheus"
    version: "release-0.12"
    dependencies: []
  }
  configuration: {
    enabled: *config["kube-prometheus"].enabled | true
    secureEndpoints: *config["kube-prometheus"].secureEndpoints | false
    trustedCACertFile: *config["kube-prometheus"].trustedCACertFile | ""
    prometheus: {
      HA: false
      HAReplicas: 1
      retentionTime: *config["kube-prometheus"].prometheus.retentionTime | "2d"
      persistence: false
      remoteWriteURL: ""
      remoteReadURL: ""
      remoteAuthType: ""
      remoteClientCertificateCertFile: ""
      remoteClientCertificateKeyFile: ""
      grafanaRemoteDatasourceName: ""
    }
    alertmanager: {
      HA: false
      HAReplicas: 1
    }
    grafana: {
      HA: false
      HAReplicas: 1
      enableMultiClusterDashboards: false
      adminUsername: config["kube-prometheus"].grafana.adminUsername
      adminPassword: config["kube-prometheus"].grafana.adminPassword
      viewerUsername: "viewer"
      viewerPassword: "viewer"
      strictRoles: false
    }
    cadvisor: {
      enabled: *config["kube-prometheus"].cadvisor.enabled | true
      version: "0.47.0"
    }
  }
  resources: {
    prometheus: {
      cpu: *config["kube-prometheus"].prometheus.resources.cpu | "300m"
      cpuLimit: *config["kube-prometheus"].prometheus.resources.cpuLimit | "1000m"
      mem: *config["kube-prometheus"].prometheus.resources.mem | "500Mi"
      memLimit: *config["kube-prometheus"].prometheus.resources.memLimit | "1500Mi"
    }
    configreloader: {
      cpu: config["kube-prometheus"].configreloader.resources.cpu | "100m"
      cpuLimit: config["kube-prometheus"].configreloader.resources.cpuLimit | "200m"
      mem: config["kube-prometheus"].configreloader.resources.mem | "100Mi"
      memLimit: config["kube-prometheus"].configreloader.resources.memLimit | "500Mi"
    }
    alertmanager: {
      cpu: *config["kube-prometheus"].alertmanager.resources.cpu | "100m"
      cpuLimit: *config["kube-prometheus"].alertmanager.resources.cpuLimit | "200m"
      mem: *config["kube-prometheus"].alertmanager.resources.mem | "100Mi"
      memLimit: *config["kube-prometheus"].alertmanager.resources.memLimit | "500Mi"
    }
    grafana: {
      cpu: *config["kube-prometheus"].grafana.resources.cpu | "100m"
      cpuLimit: *config["kube-prometheus"].grafana.resources.cpuLimit | "200m"
      mem: *config["kube-prometheus"].grafana.resources.mem | "200Mi"
      memLimit: *config["kube-prometheus"].grafana.resources.memLimit | "400Mi"
    }
    cadvisor: {
      cpu: config["kube-prometheus"].cadvisor.resources.cpu | "400m"
      cpuLimit: config["kube-prometheus"].cadvisor.resources.cpuLimit | "800m"
      mem: config["kube-prometheus"].cadvisor.resources.mem | "400Mi"
      memLimit: config["kube-prometheus"].cadvisor.resources.memLimit | "2000Mi"
    }
  }
  deploymentMethod: {}
}

let pkgElasticSearch = k.#PackageDeployment & {
  package: {
    name: "elasticsearch"
    version: "7.17.3"
    dependencies: []
  }
  configuration: {
    enabled: false
    replicas: 1
    volumeSize: "1Gi"
  }
  resources: {}
  deploymentMethod: {}
}

let pkgFileBeat = k.#PackageDeployment & {
  package: {
    let pkgFileBeatVersions = {
      "7": "7.17.3"
      "8": "8.6.2"
    }
    let _version = *config["filebeat"].elasticsearchVersion | "7"
    name: "filebeat"
    version: pkgFileBeatVersions[_version]
    dependencies: []
  }
  configuration: {
    enabled: false
    elasticsearchUrl: "http://elasticsearch-master:9200"
    elasticsearchUserName: "username"
    elasticsearchPassword: "password"
    includeUserServicesLogs: "false"
    indexPattern: "k8s-apps-logs-%{+yyyy.MM.dd}"
  }
  resources: {
    filebeat: {
      cpu: "100m"
      cpuLimit: "1000m"
      mem: "250Mi"
      memLimit: "500Mi"
    }
  }
  deploymentMethod: {}
}

let pkgKibana = k.#PackageDeployment & {
  package: {
    name: "kibana"
    version: "7.17.3"
    dependencies: []
  }
  configuration: {
    enabled: false
    elasticsearchURL: ""
    elasticsearchUsername: ""
    elasticsearchPassword: ""
  }
  resources: {}
  deploymentMethod: {}
}

let pkgKubernetesDashboard = k.#PackageDeployment & {
  package: {
    name: "kubernetes-dashboard"
    version: "2.0.0-rc5"
    dependencies: []
  }
  configuration: {}
  resources: {}
  deploymentMethod: {}
}

let pkgKeycloak = k.#PackageDeployment & {
  package: {
    name: "keycloak"
    version: "19.0.3"
    dependencies: []
  }
  configuration: {
    enabled: *config.keycloak.enabled | false
    adminUsername: *config.keycloak.adminUsername | "username"
    adminPassword: *config.keycloak.adminPassword | "password"
  }
  resources: {
    keycloak: {
      cpu: *config.keycloak.resources.cpu | "1000m"
      cpuLimit: *config.keycloak.resources.cpuLimit | "1500m"
      mem: *config.keycloak.resources.mem | "700Mi"
      memLimit: *config.keycloak.resources.memLimit | "1500Mi"
    }
  }
  resources: {}
  deploymentMethod: {}
}

let pkgAmbassador = k.#PackageDeployment & {
  package: {
    name: "ambassador"
    version: "3.6.0"
    dependencies: []
  }
  configuration: {
    XFFTrustedHops: 0
    apiext: {
      HA: false
      HAReplicas: 1
    }
  }
  resources: {
    ambassador: {
      cpu: *config.ambassador.resources.cpu | "250m"
      cpuLimit: *config.ambassador.resources.cpuLimit | "1000m"
      mem: *config.ambassador.resources.mem | "300Mi"
      memLimit: *config.ambassador.resources.memLimit | "800Mi"
    }
    apiext: {
      cpu: "0"
      cpuLimit: "0"
      mem: "0"
      memLimit: "0"
    }
  }
  deploymentMethod: {}
}

let pkgMinio = k.#PackageDeployment & {
  package: {
    name: "minio"
    version: "RELEASE.2020-03-19T21-49-00Z"
    dependencies: []
  }
  configuration: {
    enabled: false
    minioMcVersion: "RELEASE.2020-03-14T01-23-37Z"
    accessKey: "minio"
    secretKey: "kumoriminio"
  }
  resources: {}
  deploymentMethod: {}
}

let pkgEtcdBackup = k.#PackageDeployment & {
  package: {
    name: "etcdbackup"
    version: "0.0.4"
    dependencies: []
  }
  configuration: {
    enabled: false
    etcdEndpoint: "https://localhost:2379"
    schedule: "*/30 * * * *"
    deltaPeriod: "5m"
    s3Endpoint: "https://minio-mycluster.test.kumori.cloud"
    s3Region: ""
    s3AccessKey: "minio"
    s3SecretKey: "kumoriminio"
    trustedCAConfigmap: ""
    s3Bucket: "clustersbackups"
    s3Prefix: "mycluster/etcd"
  }
  resources: {}
  deploymentMethod: {}
}

let pkgPkiBackup = k.#PackageDeployment & {
  package: {
    name: "pkibackup"
    version: "0.0.0"
    dependencies: []
  }
  configuration: {
    enabled: false
    s3Endpoint: "https://minio-mycluster.test.kumori.cloud"
    s3Region: ""
    s3AccessKey: "minio"
    s3SecretKey: "kumoriminio"
    s3Bucket: "clustersbackups"
    s3Prefix: "mycluster/pki"
  }
  resources: {}
  deploymentMethod: {}
}

let pkgExternalDNS = k.#PackageDeployment & {
  package: {
    name: "externaldns"
    version: "v0.13.2"
    dependencies: []
  }
  configuration: {
    interval: "1m"
    route53: {
      awsBatchChangeSize: 100
      awsBatchChangeInterval: "6s"
    }
  }
  resources: {}
  deploymentMethod: {}
}

let pkgEventExporter = k.#PackageDeployment & {
  package: {
    name: "eventexporter"
    version: "v1.4"
    dependencies: []
  }
  configuration: {
    enabled: "false"
    HA: false
    HAReplicas: 1
    eventStoreType: ""
    eventStoreElasticsearchURL: ""
    eventStoreElasticsearchUsername: ""
    eventStoreElasticsearchPassword: ""
    eventStoreElasticsearchIndexPrefix: ""
    eventStoreElasticsearchDatePattern: ""
  }
  resources: {}
  deploymentMethod: {}
}

let pkgIngressDNS = k.#PackageDeployment & {
  package: {
    name: "ingressdns"
    version: "v0.0.0"
    dependencies: []
  }
  configuration: {}
  resources: {}
  deploymentMethod: {}
}

let pkgOutOfService = k.#PackageDeployment & {
  package: {
    name: "outofservice"
    version: "v0.0.0"
    dependencies: []
  }
  configuration: {
    hub: "docker.io"
    image: "kumoripublic/outofservice:v1.0.1"
    username: ""
    password: ""
  }
  resources: {}
  deploymentMethod: {}
}

let pkgDocker = k.#PackageDeployment & {
  package: {
    name: "docker"
    version: "5:24.0.7-1~ubuntu.20.04~focal"
    dependencies: []
  }
  configuration: {}
  resources: {}
  deploymentMethod: {}
}

let pkgCriDockerd = k.#PackageDeployment & {
  package: {
    name: "cridockerd"
    version: "0.3.8"
    dependencies: []
  }
  configuration: {}
  resources: {}
  deploymentMethod: {}
}

let pkgContainerd = k.#PackageDeployment & {
  package: {
    name: "containerd"
    version: *config.containerd.version | "1.6.24-1"
    dependencies: []
  }
  configuration: {}
  resources: {}
  deploymentMethod: {}
}

let pkgKeepalived = k.#PackageDeployment & {
  package: {
    name: "keepalived"
    version: "1:2.0.19*"
    dependencies: []
  }
  configuration: {}
  resources: {}
  deploymentMethod: {}
}

let pkgEnvoy = k.#PackageDeployment & {
  package: {
    name: "envoy"
    version: "1.25.3"
    dependencies: []
  }
  configuration: {}
  resources: {}
  deploymentMethod: {}
}

let pkgOpenEBS = k.#PackageDeployment & {
  package: {
    name: "openebs"
    version: "3.7.0"
    dependencies: []
  }
  configuration: {
    enabled: false
    ignoreDevices: ""
    classes: {
      replicated: {
        enabled: false
        type: ""
        configuration: {
          replicationLevel: 1
          device: ""
        }
      }
      localhostpath: {
        enabled: false
        type: ""
        configuration: {
          hostBaseDir: ""
        }
      }
      locallvm: {
        enabled: false
        type: ""
        configuration: {
          lvmVolumeGroup: ""
        }
      }
    }
  }
  resources: {
    replicated: {
      cpu: "250m"
      cpuLimit: "500m"
      mem: "500Mi"
      memLimit: "1Gi"
    }
    // Aux resources apply to sidecar containers
    auxReplicated: {
      cpu: "100m"
      cpuLimit: "200m"
      mem: "250Mi"
      memLimit: "500Mi"
    }
  }
  deploymentMethod: {}
}

let pkgCsiCinder = k.#PackageDeployment & {
  package: {
    name: "csi-cinder"
    version: "2.28.1"
    dependencies: []
  }
  configuration: {
    enabled: false
    cloudsYamlFile: ""
    cloudsYamlKey: ""
    classes: {}
  }
  resources: {}
  deploymentMethod: {}
}

let pkgCsiNfs = k.#PackageDeployment & {
  package: {
    name: "csi-nfs"
    version: "4.5.0"
    dependencies: []
  }
  configuration: {
    enabled: false
    classes: {}
  }
  resources: {}
  deploymentMethod: {}
}

let pkgCsiCephRBD = k.#PackageDeployment & {
  package: {
    name: "csi-ceph-rbd"
    version: "3.10.1"
    dependencies: []
  }
  configuration: {
    enabled: false
    HA: false
    cephClusters: {}
    classes: {}
  }
  resources: {}
  deploymentMethod: {}
}

let pkgDescheduler = k.#PackageDeployment & {
  package: {
    name: "descheduler"
    version: "0.25"
    dependencies: []
  }
  configuration: {
    enabled: false
    nodeSelector: "kumori/role=worker"
    interval: 1
    lowNodeUtilization: {
      enabled: false
      priorityClass: "kumori-platform"
      threshold: {
        cpu: 30
        memory: 30
        pods: 60
      }
      target: {
        cpu: 60
        memory: 60
        pods: 60
      }
    }
    spreadConstraintsViolation: {
      enabled: false
      priorityClass: "kumori-platform"
    }
  }
  resources: {
    descheduler: {
      cpu: "500m"
      cpuLimit: "500m"
      mem: "256Mi"
      memLimit: "256Mi"
    }
  }
  deploymentMethod: {}
}

let pkgClusterApi = k.#PackageDeployment & {
  package: {
    name: "clusterapi"
    version: "1.8.4"
    dependencies: []
  }
  configuration: {
    enabled: false
    providers: {
      infrastructureOpenstack: {
        enabled: false
        version: "0.10.5"
      }
      infrastructureAws: {
        enabled: false
        version: "2.6.1"
      }
    }
  }
  resources: {}
  deploymentMethod: {}
}

let pkgCertManager = k.#PackageDeployment & {
  package: {
    name: "certmanager"
    version: "1.15.1"
    dependencies: []
  }
  configuration: {
    enabled: false
  }
  resources: {}
  deploymentMethod: {}
}

let pkgCertManagerWebhookOVH = k.#PackageDeployment & {
  package: {
    name: "certmanager-webhook-ovh"
    version: "0.3.1"
    dependencies: []
  }
  configuration: {
    enabled: false
    ovhDnsCredentialsFile: ""
    certNotificationsEmail: ""
    issuers: {
      letsEncrypt: {
        enabled: false
        useStaging: false
      }
      zeroSsl: {
        enabled: false
        keyId: ""
        eabKeyHmac: ""
      }
    }
  }
  resources: {}
  deploymentMethod: {}
}

let pkgOpenstackCloudController = k.#PackageDeployment & {
  package: {
    name: "openstackcloudcontroller"
    version: "1.30.1"
    dependencies: []
  }
  configuration: {
    enabled: false
    cloudsYamlFile: ""
    cloudsYamlKey: ""
  }
  resources: {}
  deploymentMethod: {}
}

let pkgAwsCloudController = k.#PackageDeployment & {
  package: {
    name: "awscloudcontroller"
    version: "1.25.13"
    dependencies: []
  }
  configuration: {
    enabled: false
  }
  resources: {}
  deploymentMethod: {}
}

distribution: k.#Distribution & {
  ref: {
    version: [1,5,3]
    domain:  "kumori.systems"
    name: "development"
  }
  kubeVersion: "1.25.11"
  osVersion: {
    flavour: "ubuntu"
    version: "20.04"
    kernel: "5.4.0"
    systemd: ""
  }
  packages: {
    "master": [
      pkgDocker,
      pkgCriDockerd,
      pkgContainerd,
      pkgKeepalived,
      pkgEnvoy,
      pkgCalico,
      pkgKumoriVolume,
      pkgEtcdBackup,
      pkgKumoriTopology,
      pkgKumoriController,
      pkgKumoriSolutionController,
      pkgKumoriInbound,
      pkgKumoriAdmission,
      pkgKumoriAlarm,
      pkgKumoriVolumeController,
      pkgDescheduler
    ]
    "worker": [
      pkgDocker,
      pkgCriDockerd,
      pkgContainerd,
      pkgKeepalived,
      pkgEnvoy,
      pkgCalico,
      pkgKumoriVolume,
      pkgKumoriCoreDNS,
      pkgKubePrometheus,
      pkgElasticSearch,
      pkgFileBeat,
      pkgKibana,
      pkgKubernetesDashboard,
      pkgKeycloak,
      pkgMinio,
      pkgExternalDNS,
      pkgEventExporter,
      pkgIngressDNS,
      pkgAmbassador,
      pkgKumoriIpFiltering,
      pkgKumoriAuthService,
      pkgOutOfService,
      pkgOpenEBS,
      pkgCsiCinder,
      pkgCsiNfs,
      pkgCsiCephRBD,
      pkgClusterApi,
      pkgCertManager,
      pkgCertManagerWebhookOVH,
      pkgOpenstackCloudController,
      pkgAwsCloudController
    ]
  }
  packageList: [
    pkgDocker,
    pkgCriDockerd,
    pkgContainerd,
    pkgKeepalived,
    pkgEnvoy,
    pkgCalico,
    pkgKumoriCRD,
    pkgKumoriVolume,
    pkgKumoriCoreDNS,
    pkgKumoriTopology,
    pkgKumoriController,
    pkgKumoriSolutionController,
    pkgKumoriInbound,
    pkgKumoriAdmission,
    pkgKumoriAlarm,
    pkgKumoriVolumeController,
    pkgKubePrometheus,
    pkgElasticSearch,
    pkgFileBeat,
    pkgKibana,
    pkgKubernetesDashboard,
    pkgKeycloak,
    pkgMinio,
    pkgEtcdBackup,
    pkgPkiBackup,
    pkgAmbassador,
    pkgKumoriIpFiltering,
    pkgKumoriAuthService,
    pkgExternalDNS,
    pkgEventExporter,
    pkgIngressDNS,
    pkgOutOfService,
    pkgOpenEBS,
    pkgCsiCinder,
    pkgCsiNfs,
    pkgCsiCephRBD,
    pkgDescheduler,
    pkgClusterApi,
    pkgCertManager,
    pkgCertManagerWebhookOVH,
    pkgOpenstackCloudController,
    pkgAwsCloudController
  ]
}

d:distribution
