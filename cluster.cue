//  Copyright 2022 Kumori Systems S.L.
//
//  Licensed under the EUPL, Version 1.2 or – as soon they
//  will be approved by the European Commission - subsequent
//  versions of the EUPL (the "Licence");
//  You may not use this work except in compliance with the
//  Licence.
//  You may obtain a copy of the Licence at:
//
//  https://joinup.ec.europa.eu/software/page/eupl
//
//  Unless required by applicable law or agreed to in
//  writing, software distributed under the Licence is
//  distributed on an "AS IS" basis,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
//  express or implied.
//  See the Licence for the specific language governing
//  permissions and limitations under the Licence.

package clustermanager

// The name of your cluster.
// This will determine the cluster domains; for example Admission
// service will be reachable at https://devtesting.test.deployedin.cloud
clusterName: "devtesting"

// Add as many Ingress TCP ports as needed.
// IMPORTANT: The ports must be available in the host machine, since they are
// forwarded from localhost to the cluster Ingress.
ingressTcpPorts: [ 9031, 9032 ]

// Uncomment and fill this section if you want to use a rootRca (for example
// for Admission client-certificate authentication)
// clusterPkiConfig: {
//   rootCaFile: "${HOME}/certs/rca/ca.crt"
// }

// Uncomment and fill this section if you want to use your Docker Hub
// credentials, to avoid Docker Hub rate limits.
//
// dockerHubCredentialsConfig: {
//   username: "username"
//   password: "password"
// }

config: {

  "kumori-admission": {
    authenticationType: "none" // none/clientcertificate/token
    // Uncomment if authenticationType = "token"
    // tokenAuthParams: {
    //   adminPassword: "admin"
    //   develPassword: "devel"
    // }
    // Uncomment and fill if authenticationType = "clientcertificate"
    // clientCertAuthParams: {
    //   clientCertRequired: true
    //   clientCertValidateWithTrustedCA: false
    //   serverCertDir: "${HOME}/certs/leaf/admission"
    // }
  }

  // Only if authenticationType = "token"
  // keycloak: {
  //   enabled: true
  //   adminUsername: "username"
  //   adminPassword: "password"
  //   resources: {
  //     cpu: "1000m"
  //     cpuLimit: "1500m"
  //     mem: "700Mi"
  //     memLimit: "1500Mi"
  //   }
  // }

  // ambassador: {
  //   resources: {
  //     cpu: "250m"
  //     cpuLimit: "1000m"
  //     mem: "300Mi"
  //     memLimit: "800Mi"
  //   }
  // }

  "kube-prometheus": {
    enabled: false
    prometheus: {
      // retentionTime: "2d"
      // resources: {
      //   cpu: "300m"
      //   cpuLimit: "1000m"
      //   mem: "500Mi"
      //   memLimit: "1500Mi"
      // }
    }
    alertmanager: {
      // resources: {
      //   cpu: "100m"
      //   cpuLimit: "200m"
      //   mem: "100Mi"
      //   memLimit: "500Mi"
      // }
    }
    grafana: {
      adminUsername: "username"
      adminPassword: "password"
      // resources: {
      //   cpu: "100m"
      //   cpuLimit: "200m"
      //   mem: "200Mi"
      //   memLimit: "400Mi"
      // }
    }
    cadvisor: {
      enabled: false
      // resources: {
      //   cpu: "400m"
      //   cpuLimit: "800m"
      //   mem: "400Mi"
      //   memLimit: "2000Mi"
      // }
    }
  }

  // To enable Prometheus to access the Kumori service "josep/jbgtest" add:
  // "kumori-kucontroller": {
  //   externalServicesAccess: {
  //     prometheus: {
  //       source: {
  //         namespace: "monitoring"
  //         label: {
  //           key: "app.kubernetes.io/name"
  //           value: "prometheus"
  //         }
  //       }
  //       target: {
  //         deployments: [ "josep/jbgtest" ]
  //       }
  //     }
  //   }
  // }
}
